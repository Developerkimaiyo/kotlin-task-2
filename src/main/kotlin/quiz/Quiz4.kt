package quiz

import model.*

class Quiz4 {
    @OptIn(ExperimentalStdlibApi::class)
    fun groupBuyersByTown(): Map<String, List<Buyer>> {
        return buildMap{
            buyerList.forEach {
                val product = getABuyer(it.buyerId)
                put( it.buyerTown,product)
            }
        }
    }



    private fun getABuyer(buyerId: String): List<Buyer> {
        return buyerList.filter { it.buyerId == buyerId }
    }
}