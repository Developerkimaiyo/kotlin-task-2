package quiz

import model.*


class Quiz3 {

    @OptIn(ExperimentalStdlibApi::class)
    fun nameToBuyerMap(): Map<String, Buyer> {
        return buildMap{
            buyerList.forEach {
                put(it.buyerName, it)
            }
        }
    }

    @OptIn(ExperimentalStdlibApi::class)
    fun buyerToTownMap(): Map<Buyer, String>{
        return buildMap{
            buyerList.forEach {
                put(it, it.buyerTown)
            }
        }
    }

    @OptIn(ExperimentalStdlibApi::class)
    fun buyerNameToTownMap(): Map<String, String>  {
        return buildMap{
            buyerList.forEach {
                put(it.buyerName, it.buyerTown)
            }
        }
    }
}