package quiz

import MainClass
import model.*


class Quiz2 : MainClass(){

    //function to now get orderItems
    private fun  getBuyerOrderAmount(buyerId: String): List<Int> {
        return getABuyerOrderList(buyerId).flatMap(Order::orderItems).toList()
    }

    private fun getProductPrice(productIdList: List<Int>): Double {
        val productItem = ArrayList<Product>()
        productIdList.forEach{
            val product = getAProduct(it)
            productItem.add(product)
        }

        return productItem.sumOf { price -> price.cashCost }
    }

    fun moneySpentBy(buyerId: String): Double {
        val buyerProductIdList = getBuyerOrderAmount(buyerId)
        return getProductPrice(buyerProductIdList)
    }

}