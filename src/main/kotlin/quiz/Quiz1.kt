package quiz

import MainClass
import model.*

class Quiz1: MainClass() {

    fun getBuyerWithMaxOrders(): Buyer? {
        val maxOrder = orderList.maxByOrNull { it.buyerId }
        return buyerList.find { it.buyerId == maxOrder?.buyerId ?: "" }
    }

    fun getMostExpensiveProduct(buyerId:String): Product? {
        val buyerProductIdList = getABuyerProductIdList(buyerId)
        val buyerProductList =getProductListFromProductIdList(buyerProductIdList)
        return buyerProductList.maxByOrNull() { it.cashCost }
    }

    private fun getABuyerProductIdList(buyerId: String): Set<Int> {
        return getABuyerOrderList("BYERUI").flatMap(Order::orderItems).toSet()
    }

    private fun getProductListFromProductIdList(productIdList: Set<Int>):ArrayList<Product>{
        val productList = ArrayList<Product>()
        productIdList.forEach{
            val product = getAProduct(it)
            productList.add(product)
        }
        return productList
    }
}