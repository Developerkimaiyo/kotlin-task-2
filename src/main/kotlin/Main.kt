import model.*
import quiz.*

open class MainClass {
    fun getAProduct(productId: Int): Product {
        val product = productList.find { it.productId == productId }
        return product!!
    }

    fun getABuyerOrderList(buyerId: String): List<Order> {
        return orderList.filter { it.buyerId == buyerId }
    }
}

fun main() {
    val quiz1 = Quiz1()
    val quiz2 = Quiz2()
    val quiz3 = Quiz3()
    val quiz4 = Quiz4()


    println(quiz1.getBuyerWithMaxOrders())
    println( quiz1.getMostExpensiveProduct("BYERUI"))
    println( quiz2.moneySpentBy("BYERUI"))
    println(quiz3.nameToBuyerMap())
    println(quiz3.buyerToTownMap())
    println(quiz3.buyerNameToTownMap())
    println(quiz4.groupBuyersByTown())
}