package model

data class Order(val orderId: String, val orderAmount: Double, var buyerId:String, val orderItems:List<Int>)

val orderList = listOf(
    Order("abidieid", 9000.98 , "BNMHJK", listOf(12345, 78906, 78906)),
    Order("abcvbcvr", 8000.00 , "BKLJMN", listOf(98765, 123450)),
    Order("vbgfhbcn", 45000.89, "BNHGHJ", listOf(78906, 98765, 12345)),
    Order("mnhjklir", 1200.87 , "BYERUI", listOf(123450,12345, 123450)),
    Order("mnhvcx",   2200.75 , "BNMHJK", listOf(78906, 12345, 123450)),
    Order("mnhrnm",   3200.87 , "BYERUI", listOf(98765,12345)),
    Order("jkvrnm",   2700.00 , "BYERUI", listOf(78906))
)