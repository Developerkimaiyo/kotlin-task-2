package model

data class Product(val productName: String, val cashCost: Double, val productCategory:String, val productId:Int)

val productList = listOf(
    Product("Rice", 1445.00 , "Food", 12345),
    Product("Sugar", 1600.50, "Food", 123450),
    Product("Confidence", 1000.00, "Beauty", 98765),
    Product("Amarula", 2700.00, "Drinks", 78906)
)