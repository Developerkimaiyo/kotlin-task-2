package model

data class Buyer(val buyerId: String, val buyerName: String, val buyerTown:String)


val buyerList =listOf(
    Buyer("BYERUI", "Tindi Shiro" , "Westlands"),
    Buyer("BNHGHJ", "Shiro Tindi" , "Kikuyu"),
    Buyer("BKLJMN", "Max Kim" , "Kahawa"),
    Buyer("BNMHJK", "Kim Max" , "Donholm"),
    Buyer("BNHG9I", "Ray Jay" , "Kikuyu")
)